node default {

#### APACHE 2 ####
class {'apache':}

  apache::vhost {'sharp-mcnulty.dgr.ovh':
  servername    => 'sharp-mcnulty.dgr.ovh',
  port          => '80',
  docroot       => '/data/www',
  docroot_owner => 'www-data',
  docroot_group => 'www-data',
  }

#### MY SQL ####
classe {'mySQL':}
mysql::db { ('puppetdb'):
  user      => 'root',
  password  => 'Sdfghjkl39!',
  host      => 'localhost',
  grant     => ['SELECT', 'UPDATE']
}

#### PARE FEU ####
class {'ufw':}

ufw::allow {'ssh':
  port => '22'

}
ufw::allow {'http':
  port => '80'
  }
ufw::allow {'https':
  port => '443' 
    }

#### PHP ####

include '::php'
class { 'php': }
#### Creation Template ####

$template = @(END)
<%=@fqdn%> <%=@networking['interfaces']['eth0']['ip']%>
END

file{'/data':
ensure  => 'directory',
owner   => 'www-data',
group   => 'www-data',
mode    => '0750',
}

file{'/data/www':
ensure  => 'directory',
owner   => 'www-data',
group   => 'www-data',
mode    => '0750',
}

file{'/data/www/index.php':
ensure  => 'file',
content => inline_template($template),
owner   => 'www-data',
group   => 'www-data',
mode    => '0750',

}


#### GRAFANA ####

class { 'grafana':
      cfg => {
        app_mode => 'production',
        server   => {
          http_port     => 8080,
        },
        database => {
          type          => 'mysql',
          host          => 'localhost',
          name          => 'grafana',
          user          => 'root',
          password      => 'Sdfghjkl39',
        },
        users    => {
          allow_sign_up => false,
        },
      },
    }

#### TELEGRAF ####
    class { 'telegraf':
    hostname => $facts['hostname'],
    outputs  => {
        'influxdb' => [
            {
                'urls'     => [ "http://influxdb0.${facts['domain']}:8086", "http://influxdb1.${facts['domain']}:8086" ],
                'database' => 'telegraf',
                'username' => 'telegraf',
                'password' => '',
            }
        ]
    },
}

#### INFLUX DB ####
class {'influxdb::server':
  graphite_options => {
    enabled           => true,
    database          => graphite,
    bind-address      => ':2003',
    protocol          => tcp,
    consistency-level => 'one',
    name-separator    => '.',
    batch-size        => 1000,
    batch-pending     => 5,
    batch-timeout     => '1s',
  },
 }
}
